package com.duvangiraldom.products.application;

import com.duvangiraldom.products.model.Product;

import java.util.List;
import java.util.Optional;

public interface IProductService {

    List<Product> getAll();

    Optional<Product> getById(String id);

    Product insert(Product product);

    Product update(Product product);

    Product delete(String id);
}

package com.duvangiraldom.products.application.services;

import com.duvangiraldom.products.application.IProductService;
import com.duvangiraldom.products.model.Product;
import com.duvangiraldom.products.persistence.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements IProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Optional<Product> getById(String id) {
        return productRepository.findById(id);
    }

    @Override
    public Product insert(Product product) {
        return productRepository.insert(product);
    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product delete(String id) {
        Product product = productRepository.findById(id).get();
        productRepository.delete(product);
        return product;
    }
}

package com.duvangiraldom.products.controller;

import com.duvangiraldom.products.application.IProductService;
import com.duvangiraldom.products.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private IProductService productService;

    @GetMapping("/")
    public List<Product> getAll(){
        return productService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Product> getById(@PathVariable String id){
        return productService.getById(id);
    }

    @PostMapping("/")
    public Product insert(Product product){
        return productService.insert(product);
    }

    @PutMapping("/")
    public Product update(Product product){
        return productService.update(product);
    }

    @DeleteMapping("/{id}")
    public Product delete(@PathVariable String id){
        return productService.delete(id);
    }
}
